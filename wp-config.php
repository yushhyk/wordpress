<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpresshw' );

/** MySQL database username */
define( 'DB_USER', 'wizard' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wizard' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Qe]i0I|AeBx2u-4F#}iMe2XAJB~k[__Tm4voW3i;pGFQ&%%Wu*afG~3lDj4v`I3=' );
define( 'SECURE_AUTH_KEY',  'e<W)VuqP`!8g_D)*/:BFRs:[~vM2>0Th5B@Ud7)H6d.:36iwgGW3rq^l*>G.98.Z' );
define( 'LOGGED_IN_KEY',    'SCNV`gWJ{g)owl*G)@TlpJaFb;qmjC@{+X;%@@#JaRV2W3/&IhlWOd}@tHg&,nZb' );
define( 'NONCE_KEY',        '<)q.^-5A1q-=mmwVk3}TE_Ub~xuj&k*i@5u(`~(i.Rkyu&&+GXY$/UKJQ/@kpzU$' );
define( 'AUTH_SALT',        'R&Eq?1.}ql{=W(3)V<4A/F7Jng[l^yW>n?T-tCj:yd$tARv36!ux!!eb.D/>+W%1' );
define( 'SECURE_AUTH_SALT', 'mh S58E_,<{iB)7aw9YM<<c]Bri|[tIX:Be3aiFLpi?{TQ| PT#g6(3C]~b3x0I=' );
define( 'LOGGED_IN_SALT',   'm>j e>=jLp,W??!dt.b|Wq$EaZ2)s6/0v&2,pYO)M]7cA6<k]`p6dp.N!&K)h0hO' );
define( 'NONCE_SALT',       '(*w<tCv|R0D~YseET-.0$oEW2kwg+BP-8K1=1uP#+Z[bS(s![W!8tGY^1TKNgw)=' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
