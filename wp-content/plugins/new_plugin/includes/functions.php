<?php

add_action('admin_menu', 'new_admin_link');

function new_admin_link() {
    add_menu_page(
        'New link',
        'First plugin',
        'manage_options',
        '/includes/index.php'
    );
}
